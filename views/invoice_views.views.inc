<?php

/**
 * Implements hook_views_data()
 */
function invoice_views_views_data() {
  $data['invoice_invoices']['table']['group'] = t('Invoice');
  $data['invoice_invoices']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Invoice'),
    'help' => t('Invoice table contains all invoice data.'),
    'weight' => -10,
  );
  $data['invoice_invoices']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['invoice_invoices']['nid'] = array(
    'title' => t('Invoice'),
    'help' => t('Invoice reference'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Invoice link'),
    ),
  );
  $data['invoice_invoices']['pay_status'] = array(
    'title' => t('Pay status'), 
    'help' => t('Current status of the invoice payment'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['invoice_items']['table']['group'] = t('Invoice');
  $data['invoice_items']['table']['base'] = array(
    'field' => 'iid',
    'title' => t('Invoice items'),
    'help' => t('Invoice table contains all invoice item data.'),
    'weight' => -10,
  );
  $data['invoice_items']['table']['join'] = array(
    'node' => array(
      'left_table' => 'invoice_invoices',
      'left_field' => 'iid',
      'field' => 'invoice_id',
    ),
  );
  $data['invoice_items']['iid'] = array(
    'title' => t('Invoice items'),
    'help' => t('Invoice reference'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'iid',
      'handler' => 'views_handler_relationship',
      'label' => t('Invoice items link'),
    ),
  );
   $data['invoice_items']['unitcost'] = array(
    'title' => t('Unit costs'), 
    'help' => t('Costs of unit items in the invoice'), 
    'field' => array(
      'handler' => 'invoice_views_unit_cost_handler', // Use the custom handler
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
 
  return $data;
}

/**
 * Implements hook_views_handlers()
 */
function invoice_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'invoice_views') . '/views',
    ),
    'handlers' => array(
      'invoice_views_unit_cost_handler' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
