<?php

class invoice_views_unit_cost_handler extends views_handler_field {
  // Rewrite the field to be multiplied by the amount of items ordered
  function query() {
    $this->ensure_my_table();

    // Add (or not) VAT, depending on the display options
    if ($this->options['vat'])
      $formula = 'SUM((100 + ' . $this->table_alias . '.vat)/100 * ' . $this->table_alias . '.quantity * ' . $this->table_alias . '.unitcost)';
    else
      $formula = 'SUM(' . $this->table_alias . '.quantity * ' . $this->table_alias . '.unitcost)';

    $this->field_alias = $this->query->add_field(NULL, $formula, $this->table . '_' . $this->field);
  }

  // Display a rounded version of the value
  function render($values) {
    $value = $this->get_value($values);
    $value = abs(round($value * 100) / 100);
  }

  // Add options to the form
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['vat'] = array(
      '#title' => t('Include VAT'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['vat'],
    );
  }

  // Make the options known to Views
  function option_definition() {
    $options = parent::option_definition();
    $options['vat'] = array('default' => TRUE);
    return $options;
  }
}
